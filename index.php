<?php get_header(); ?>


<section class="page-banner press-center-banner hide-for-small-only">

	<div class="feat-img"></div>

</section><!-- // page-banner press-center-banner -->


<section class="page-title">

	<h1><?php echo get_queried_object()->post_title;?></h1>

</section><!-- // page-banner -->


<?php get_template_part('parts/content', 'breadcrumbs'); ?>


<div class="press-center">

	<div class="row">

		<?php get_sidebar(); ?>

		<main class="small-12 medium-8 columns" role="main">

			<div class="article-excerpts row" data-equalizer data-equalize-on="medium" data-equalize-by-row="true">

				<?php 
					$starting_no = 10;
					$args = array( 
					'post_type' => 'post', 
					'posts_per_page' => $starting_no
					); 

				$loop = new WP_Query( $args ); 
					$count = $loop->found_posts;
				while ( $loop->have_posts() ) : $loop->the_post(); 
				?>
				
				<div class="article-excerpt small-10 small-centered medium-6 medium-uncentered columns" data-equalizer-watch>

					<?php get_template_part( 'parts/loop', 'archive' ); ?>

				</div><!-- // article-excerpt -->

				<?php endwhile; ?>	
					<?php if($starting_no < $count): echo do_shortcode( '[ajax_load_more repeater="standard" id="" container_type="div" post_type="post" posts_per_page="4" offset="'.$starting_no.'" custom_args="data-equalizer, data-equalize-on=medium, data-equalize-by-row=true" pause="true" scroll="false" transition="fade" transition_speed="500" images_loaded="true" button_label="show more"]' ); endif; ?>

				<?php //joints_page_navi(); ?>

				<?php //else : ?>

				<?php //get_template_part( 'parts/content', 'missing' ); ?>

				<?php //endif; ?>

				<?php //wp_reset_postdata(); // reset the query ?>

			</div><!-- // article-excerpts row -->

		</main>
	</div><!-- // row -->

</div><!-- // press-center -->


<?php get_footer(); ?>