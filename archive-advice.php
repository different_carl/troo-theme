<?php 
$counter=0;
?>

<?php get_header(); ?>
<?php $x = get_field('ea_title','options'); if($x): else: $x = 'Energy advice. Save smarter.';endif; ?>


<?php get_template_part('parts/content', 'feat-img'); ?>

<section class="page-title">

	<h1><?php echo $x;?></h1>

</section><!-- // page-banner -->


<?php get_template_part('parts/content', 'breadcrumbs'); ?>


<div class="energy-advice row ">

	<div class="energy-feat-article column" data-equalizer>
		<!-- <div class="row "> -->
			<?php 
				$args = array( 
					'post_type' => 'advice', 
					'posts_per_page' => 1, 
					'orderby' => 'modified',
					'meta_query' => array(
						array(
							'key' => 'sticky_article',
							'compare' => '=',
							'value' => '1' // the Yes option is selected, to be featured
						)
					)
				); 
				$loop = new WP_Query( $args ); 
				while ( $loop->have_posts() ) : $loop->the_post(); 
				$exclude=$post->ID;
			?>
			<?php get_template_part( 'parts/loop', 'advice-featured' ); ?>
			<?php endwhile; ?>
		<!-- </div> -->
		</div>
	</div>


	<div class="energy-article-feed  section-article row">

		<main id="main" class="column " role="main">

			<div class="" data-equalizer data-equalize-on="medium" data-equalize-by-row="true" style="float:left;">

				<?php 
					// to ensure we capture those that are not set, and those that are now unset
				
				$starting_no = 6;
					$args = array( 
						'post_type' => 'advice', 
					'posts_per_page' => $starting_no,
						'post__not_in' => array($exclude)
					); 
					$loop = new WP_Query( $args ); 
					
					$count = $loop->found_posts;
					$class = the_count($count);
					while ( $loop->have_posts() ) : $loop->the_post(); 
						if($counter == 3):echo '</div><div style="float:left;">';$counter=0;endif;
						$counter++;
				?>
				
				<article id="post-<?php the_ID(); ?>" <?php post_class('small-12 '.$class.' columns'); ?> role="article" data-equalizer-watch>
					<?php get_template_part( 'parts/loop', 'home' ); ?>
				</article><!-- // article -->

				<?php endwhile; ?>

			</div><!-- // row -->

			<?php  if($starting_no < $count): echo do_shortcode( '[ajax_load_more id="LoadMoreEqualize" container_type="div" css_classes="medium-collapse " post_type="advice" posts_per_page="'.$starting_no.'" exclude="'.$exclude.'" offset="'.$starting_no.'" custom_args="data-equalizer, data-equalize-on=medium, data-equalize-by-row=true" pause="true" scroll="false" transition="fade" transition_speed="500" images_loaded="true" button_label="show more"]' );endif; ?>

			<?php /* ?>
			<div class="centered-cta small-12 medium-12 columns">
				<a class="button" href="#">show more</a>
			</div>
			<?php */ ?>

		</main>
	</div><!-- // row -->

</div><!-- // energy-advice -->


<?php get_footer(); ?>



<?php get_footer(); ?>