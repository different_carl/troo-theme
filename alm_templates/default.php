<?php /* ref: https://connekthq.com/plugins/ajax-load-more/docs/repeater-templates/ */ ?>

<?php
	if(get_post_type()=='post'):
		?>

<div class="article-excerpt small-10 small-centered medium-6 medium-uncentered columns" data-equalizer-watch>

	<?php get_template_part( 'parts/loop', 'archive' ); ?>

</div><!-- // article-excerpt -->
<?php else:?>
<article id="post-<?php the_ID(); ?>" <?php post_class('small-12 medium-4 columns'); ?> role="article" data-equalizer-watch>
	
	<header class="article-header">
		
		<?php 
			$excerpt = get_field( "excerpt" );
			if( function_exists( 'has_post_thumbnail' ) && has_post_thumbnail() ) { 
				$featuredImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
		?>
		<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="feat-img" style="background-image:url(<?php echo $featuredImage; ?>);"></a>
		<?php } else { ?>
		<?php } ?>

		<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>

	</header> <!-- end article header -->

	<section class="entry-content" itemprop="articleBody">

		<?php if($excerpt):?>
				<?php 
				echo '<p>'.$excerpt.'<a href="'. get_permalink($post->ID) . '" title="'. __('Read ', '') . get_the_title($post->ID).'">'. __(' Read more', '') .'</a></p>';
				?>
		<?php else: 
				echo '<p><a href="'. get_permalink($post->ID) . '" title="'. __('Read ', '') . get_the_title($post->ID).'">'. __(' Read more', '') .'</a></p>';
		?>
		<?php endif; ?>
		
	</section><!-- // entry-content -->

</article><!-- // article -->

<?php endif;?>