<?php

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Troo details',
		'menu_title'	=> 'Troo details',
		'menu_slug' 	=> 'troo-general-details',
		'capability'	=> 'edit_pages',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Archives',
		'menu_title'	=> 'Archives',
		'parent_slug'	=> 'troo-general-details',
		'capability'	=> 'edit_pages',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Press details',
		'menu_title'	=> 'Press details',
		'parent_slug'	=> 'troo-general-details',
		'capability'	=> 'edit_pages',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Reasons to believe',
		'menu_title'	=> 'Reasons to believe',
		'parent_slug'	=> 'troo-general-details',
		'capability'	=> 'edit_pages',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'troo-general-details',
		'capability'	=> 'edit_pages',
	));

	
}

add_filter('acf/validate_value/type=wysiwyg', 'my_acf_validate_value', 10, 4);

function my_acf_validate_value( $valid, $value, $field, $input ){
	$name = $field['name'];
    if($name == 'about_text'):
    	$count = 700;
    elseif($name == 'whoweare_text'): 
    	$count = 500;
	elseif($name == 'press_about'):     
    	$count = 240;
    else:
    	$count = 'ignore';
    endif;
    // bail early if value is already invalid
    if( !$valid ) {
        
        return $valid;
        
    }

    // return $field;
    if($count != 'ignore'):

		$stripped = strip_tags($value);
		// $nohtml = count($stripped);
    	if( strlen($stripped) > $count ) {
        
       		$valid = 'You can\'t enter more than '.$count.' chars';
        
    	}
    endif;
    
    
    return $valid;
    // return 'hi '.$field;
    // print_r($field['name']);
    
    
}