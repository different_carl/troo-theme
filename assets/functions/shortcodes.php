<?php
/**
* Plugin Name: Shortcake UI
* Plugin URI: 
* Description: Try Shortcake with pull-quite shortcode
* Version: 1.0.0
* Author: Mte90
* License: GPL2
*/

add_action( 'init', function() {



    add_shortcode( 'button', function( $attr, $content = '' ) {

        $attr = wp_parse_args( $attr, array(
            'source' => ''
        ) );
        ob_start();

        ?>



        <?php if(! empty($attr['url']) ):echo '<span class="centered-cta small-12 medium-12 columns"><a href="'.$attr['url'].'" class="button" target="_blank">'.$attr['txt'].'</a></span>'; endif; 

            return ob_get_clean();
        } );

   


     /** BUTTON UI
     */
    shortcode_ui_register_for_shortcode(
        'button',
        array(

            // Display label. String. Required.
            'label' => 'Button',

            // Icon/image for shortcode. Optional. src or dashicons-$icon. Defaults to carrot.
            'listItemImage' => 'dashicons-external',

            // Available shortcode attributes and default values. Required. Array.
            // Attribute model expects 'attr', 'type' and 'label'
            // Supported field types: text, checkbox, textarea, radio, select, email, url, number, and date.
            'attrs' => array(
                array(
                    'label' => 'Button text',
                    'attr'  => 'txt',
                    'type'  => 'text',
                ),
                array(
                    'label' => 'Link',
                    'attr'  => 'url',
                    'type'  => 'url',
                )

            ),
        )
    );

} );

?>