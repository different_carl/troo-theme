jQuery(document).foundation();

// jQuery(document).ready(function(jQuery) {
	
// 		// when the plugin Ajax Load More completes a load...
// 		jQuery.fn.almComplete = function(alm){
// 			console.log("Ajax Load More Complete!");
// 			// new Foundation.Equalizer(jQuery('#ajax-load-more')).applyHeight(); 
// 			var fooEq = new Foundation.Equalizer($("#ajax-load-more"));
// 			fooEq.getHeights(); 
// 			// fooEq.applyHeight(); 
// 			fooEq.applyHeightByRow(); 
// 			// Foundation.reInit('equalizer');
// 			// $('#ajax-load-more').foundation();
// 			 // new Foundation.Equalizer($("#ajax-load-more")).getHeights(); 
// 		};

// });
/*
These functions make sure WordPress
and Foundation play nice together.
*/
jQuery(function ($) {





$(window).resize(function(){
  equalheight('.equ');
  // Resize homepage header section to equal viewport height
  var total = $(window).height() - $('.header').height();
  $('.home-hero').css('height', total);

  // Equal heights
  // equalheight('.compare-list .equal');
  // equalheight('.compare-list .one');
  // equalheight('.compare-list .two');
  // equalheight('.compare-list .three');
  // equalheight('.compare-list .four');
  // equalheight('.compare-list .five');
  // equalheight('.compare-list .six');
});

$(document).ready(function($) {



  
  equalheight('.equ');
  var total = $(window).height() - $('.header').height();
  
  // Remove empty P tags created by WP inside of Accordion and Orbit
  $('.accordion p:empty, .orbit p:empty').remove();

	 // Makes sure last grid item floats left
	$('.archive-grid .columns').last().addClass( 'end' );

	// Adds Flex Video to YouTube and Vimeo Embeds
  $('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
    if ( $(this).innerWidth() / $(this).innerHeight() > 1.5 ) {
      $(this).wrap("<div class='widescreen flex-video'/>");
    } else {
      $(this).wrap("<div class='flex-video'/>");
    }
  });

  $('.home-hero').css('height', total);
    
});



equalheight = function(container){


var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPosition = $el.position().top;

   if (currentRowStart != topPosition) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPosition;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  // equalheight('.compare-list .equal');
  // equalheight('.compare-list .one');
  // equalheight('.compare-list .two');
  // equalheight('.compare-list .three');
  // equalheight('.compare-list .four');
  // equalheight('.compare-list .five');
  // equalheight('.compare-list .six');
});





});
