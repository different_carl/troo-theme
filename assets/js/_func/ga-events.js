jQuery(function ($) {
		$('.ga--track--signup').on('click', function() {
			var i = $(this).data('g-a');
			ga('send', 'event', 'Sign up', 'click', i);
		});

		$('.ga--track--footer').on('click', function() {
			ga('send', 'event', 'Sign up', 'click', 'Sign up - text link - footer');
		});

		$('.ga--track--main-nav').on('click', function() {
			ga('send', 'event', 'Sign up', 'click', 'Sign up - text link - main-nav');
		});

});