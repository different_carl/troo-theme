<?php
$m = get_post_type();
if( $m == 'advice' ) {
	$a = get_field('ea_feat-img','option');
	$featuredImage = $a['url'];
} elseif( $m == 'press' ) {
	$a = get_field('pr_feat-img','option');
	$featuredImage = $a['url'];
} else {
	$featuredImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
}

?>

<section class="page-banner">

		<?php 
			 if( $featuredImage ) { 
		?>
			<div class="feat-img hide-for-small-only" style="background-image:url(<?php echo $featuredImage; ?>);"></div>
	<?php } else { ?>
			
	<?php } ?>

</section><!-- // page-banner -->