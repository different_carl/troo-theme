<article id="post-<?php the_ID(); ?>" <?php post_class('featured-article '); ?> role="article" data-equalizer-watch>
	
	<?php 
		$excerpt = get_field( "excerpt" );
		if( function_exists( 'has_post_thumbnail' ) && has_post_thumbnail() ) { 
			$featuredImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
	?>
	<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="article-feat-img small-12 medium-6 columns" style="background-image:url(<?php echo $featuredImage; ?>);">
	</a><!-- // article-feat-img small-12 medium-6 columns -->
	<?php } else { ?>
	<a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="article-feat-img"></a>
	<?php } ?>

	<div class="small-12 medium-6 columns">

		<header class="article-header">
			<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
		</header> <!-- end article header -->
		<section class="entry-content" itemprop="articleBody">

		<?php if($excerpt):?>
				<?php 
				echo '<p>'.$excerpt.'<a href="'. get_permalink($post->ID) . '" title="'. __('Read ', '') . get_the_title($post->ID).'">'. __(' Read more', '') .'</a></p>';
				?>
		<?php else: 
				echo '<p><a href="'. get_permalink($post->ID) . '" title="'. __('Read ', '') . get_the_title($post->ID).'">'. __(' Read more', '') .'</a></p>';
		?>
		<?php endif; ?>
		
	</section><!-- // entry-content -->

	</div><!-- small-12 medium-6 columns -->

</article><!-- // article -->