<?php
	$x = get_field('press-on','options');
	if($x == 0):
	header("Location: ".home_url()); //REMOVE THIS WHEN PRESS READY
	endif;
?>
<?php get_header(); ?>


<section class="page-banner single-banner">
	<?php 
		if( function_exists( 'has_post_thumbnail' ) && has_post_thumbnail() ) {
			$featuredImage = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
	?>
	<div class="feat-img" style="background-image:url(<?php echo $featuredImage; ?>);"></div><!-- // feat-img -->
	<?php } ?>
</section><!-- // page-banner single-banner -->


<?php get_template_part('parts/content', 'breadcrumbs'); ?>


<div class="row">
	<div class="small-10 small-centered medium-8 columns">

		<main class="main-single" role="main">

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<?php get_template_part( 'parts/loop', 'single' ); ?>

			<?php endwhile; else : ?>

			<?php get_template_part( 'parts/content', 'missing' ); ?>

			<?php endif; ?>

		</main><!-- end #main -->


		<div class="related-articles row">
			<div class="small-12 columns">

				<?php joints_related_posts(); ?>

			</div>
		</div><!-- // row -->

	</div>
</div><!-- // row -->


<?php get_footer(); ?>